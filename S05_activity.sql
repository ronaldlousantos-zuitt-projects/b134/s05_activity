1.
SELECT customerName FROM customers WHERE country = "Philippines";

2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

3.
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

4.
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

5.
SELECT customerName FROM customers WHERE state IS NULL;

6.
SELECT firstName, lastName, email FROM employees Where lastName = "Patterson" AND firstName = "Steve";

7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

8.
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

9.
SELECT customerNumber FROM orders WHERE comments LIKE "%dhl%";

10.
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

11.
SELECT DISTINCT country FROM customers;

12.
SELECT DISTINCT status FROM orders;

13.
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

14.
SELECT employees.firstName, employees.Lastname, offices.city FROM employees JOIN offices ON employees.officeCode = offices.officeCode;

15.
SELECT customers.customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

16.
SELECT products.productName, customers.customerName FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = "Baane Mini Imports";

17.
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country AS country_of_transaction FROM
customers
JOIN employees ON customers.salesRepEmployeeNumber=employees.employeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.country = customers.country;

18.
SELECT employees.lastName, employees.firstName FROM employees WHERE employees.reportsTo = (SELECT
employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow");

19.
SELECT products.productName, products.MSRP FROM products WHERE products.MSRP = (SELECT MAX(MSRP) FROM products);

20.
SELECT COUNT(*) from customers WHERE customers.country = "UK";

21.
SELECT  productlines.productLine, count(*) AS number_of_products_in_product_line FROM products
JOIN productlines ON products.productLine =productlines.productLine
GROUP BY productlines.productLine;

22.
SELECT employees.firstName, employees.lastName, count(*) AS number_of_customers_served
FROM customers
JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
GROUP BY salesRepEmployeeNumber;

23.
SELECT products.productName, products.quantityInStock FROM products
WHERE productLine = "Planes" AND quantityInStock < 1000;
